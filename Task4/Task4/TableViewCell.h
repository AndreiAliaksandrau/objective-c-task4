//
//  TableViewCell.h
//  Task4
//
//  Created by Андрей Александров on 4/1/18.
//  Copyright © 2018 AndreiAliaksandrau. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *row;

@end
