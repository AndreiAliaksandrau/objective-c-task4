//
//  TableViewCell.m
//  Task4
//
//  Created by Андрей Александров on 4/1/18.
//  Copyright © 2018 AndreiAliaksandrau. All rights reserved.
//

#import "TableViewCell.h"

@implementation TableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.selectionStyle = UITableViewCellSelectionStyleGray;
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
}

@end
