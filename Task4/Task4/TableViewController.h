//
//  TableViewController.h
//  Task4
//
//  Created by Андрей Александров on 4/1/18.
//  Copyright © 2018 AndreiAliaksandrau. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TableViewCell.h"

@interface TableViewController : UITableViewController

@property (strong, nonatomic) NSDictionary *data;
@property (strong, nonatomic) NSArray *sectionTitles;

@end
