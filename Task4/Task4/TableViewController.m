//
//  TableViewController.m
//  Task4
//
//  Created by Андрей Александров on 4/1/18.
//  Copyright © 2018 AndreiAliaksandrau. All rights reserved.
//

#import "TableViewController.h"
#import "TableViewCell.h"

@interface TableViewController ()
- (NSString *) getSelectedRow:(NSIndexPath *)indexPath;
- (void) backHandler:(NSString *)eventName;
@property (strong, nonatomic) IBOutlet UITableView *table;

@end

@implementation TableViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    [self.table registerNib:[UINib nibWithNibName:@"TableViewCell" bundle:nil] forCellReuseIdentifier:@"Cell"];
    
    self.data = @{@"Navigation actions" : @[@"Back", @"Push", @"Present modal"],
                  @"Log actions" : @[@"Log section number", @"Log row number", @"Log table frame", @"Log cell frame"]};
    self.sectionTitles = [self.data allKeys];
    
    self.tableView.rowHeight = 60;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.sectionTitles.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSString *sectionTitle = [self.sectionTitles objectAtIndex:section];
    NSArray *data = [self.data objectForKey:sectionTitle];
    
    return data.count;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return [self.sectionTitles objectAtIndex:section];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    TableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    NSString *selectedRow = [self getSelectedRow:indexPath];

    cell.row.text = selectedRow;
    
    switch (indexPath.section) {
        case 0:
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            break;
        case 1:
            cell.accessoryType = UITableViewCellAccessoryDetailButton;
            break;
        default:
            break;
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSString *selectedRow = [self getSelectedRow:indexPath];
    
    switch (indexPath.section) {
        case 1: {
            NSLog(@"%@", selectedRow);
        }
            break;
        default:
            break;
    }
    
    [self backHandler:selectedRow];
}

- (NSString *) getSelectedRow:(NSIndexPath *)indexPath {
    NSString *sectionTitle = [self.sectionTitles objectAtIndex:indexPath.section];
    NSArray *data = [self.data objectForKey:sectionTitle];
    NSString *selectedRow = [data objectAtIndex:indexPath.row];
    
    return selectedRow;
}

- (void) backHandler:(NSString *)eventName {
    if ([eventName isEqualToString:@"Push"]) {
        UITableViewController *uc = [self.storyboard instantiateViewControllerWithIdentifier:@"TableViewController"];
        [self.navigationController pushViewController:uc animated:YES];
    }
    else if ([eventName isEqualToString:@"Back"]) {
        [self.navigationController popViewControllerAnimated:YES];
        
        if (self.presentingViewController != nil) {
            [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
        }
    }
    else if ([eventName isEqualToString:@"Present modal"]) {
        UITableViewController *uc = [self.storyboard instantiateViewControllerWithIdentifier:@"TableViewController"];
        [self.navigationController presentViewController:uc animated:YES completion:nil];
    }
}

//- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
//    return 60;
//}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
